<div class="h3">Usage of plugin questionExtraSurvey</div>
<div class='table-responsive'>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>QID</th>
                <th>In survey</th>
                <th>In survey name</th>
                <th>Related survey</th>
                <th>Related survey name</tht>
            </tr>
        </thead>
        <tbody>
            <?php foreach($aQidSurveys as $aQidSurvey) : ?>
            <tr>
               <td><?= CHtml::link(
                    $aQidSurvey['qid'],
                    array_merge($questionlink, array('surveyid' => $aQidSurvey['sid'], 'gid' => $aQidSurvey['gid'], 'qid' => $aQidSurvey['qid']))
                ); ?></td>
                <td><?= CHtml::link(
                    $aQidSurvey['sid'],
                    array_merge($surveylink, array('surveyid' => $aQidSurvey['sid']))
                ); ?></td>
                <td><?= CHtml::encode($aQidSurvey['sidname']) ?></td>
                <td><?= CHtml::link(
                    $aQidSurvey['withsid'],
                    array_merge($surveylink, array('surveyid' => $aQidSurvey['withsid']))
                ); ?></td>
                <td><?= CHtml::encode($aQidSurvey['withsidname']) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
